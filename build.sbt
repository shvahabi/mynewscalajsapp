enablePlugins(ScalaJSPlugin)

name := "New Scala.js App Template"
scalaVersion := "3.0.0" // or any other Scala version >= 2.11.12

// This is an application with a main method
scalaJSUseMainModuleInitializer := false

libraryDependencies += ("org.scala-js" %%% "scalajs-dom" % "1.1.0").cross(CrossVersion.for3Use2_13)
jsEnv := new org.scalajs.jsenv.jsdomnodejs.JSDOMNodeJSEnv()

libraryDependencies += ("com.lihaoyi" %%% "utest" % "0.7.4" % "test").cross(CrossVersion.for3Use2_13)
testFrameworks += new TestFramework("utest.runner.Framework")
