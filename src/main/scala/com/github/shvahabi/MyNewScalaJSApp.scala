package com.github.shvahabi

import scala.scalajs.js
import org.scalajs.dom.ext._

import org.scalajs.dom
import org.scalajs.dom.document
import scala.scalajs.js.annotation.JSExportTopLevel

object MyNewScalaJSApp {
  
  @JSExportTopLevel("greetings")
  def greetings(): Unit = {
    val paragraph = document.createElement("p")
    paragraph.textContent = "Hello world!"
    document.body.appendChild(paragraph)
  }
}
